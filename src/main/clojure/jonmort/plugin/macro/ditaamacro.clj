(ns jonmort.plugin.macro.ditaamacro
  (:use [ring.util.codec :only [url-encode]]
        [jonmort.plugin.repl.osgi :only [service]])
  (:require [clojure.data.codec.base64 :as b64])
  (:import (com.atlassian.confluence.macro Macro))
  (:gen-class :implements [com.atlassian.confluence.macro.Macro] :main false))

(defn make-query-string [m & [encoding]]
  (let [s #(if (instance? clojure.lang.Named %) (name %) %)
        enc (or encoding "UTF-8")]
    (->> (for [[k v] m]
           (str (url-encode (s k) enc)
                "="
                (url-encode (str v) enc)))
         (interpose "&")
         (apply str))))

(defn build-url [url-base query-map & [encoding]]
  (str url-base "?" (make-query-string query-map encoding)))

(defn execute [params body context]
  (let [data (-> (.getBytes body "UTF-8")
                 (b64/encode)
                 (String. "UTF-8"))
        params (assoc  (into {} params) :data data)
        base-url (.getBaseUrl (service "com.atlassian.sal.api.ApplicationProperties"))
        url (build-url (str base-url "/plugins/servlet/ditaa/image")
                       params)]
    (str "<img src=\"" url "\">" )))


(defn -execute [_ params body context]
  (execute params body context))

(defn -getBodyType [_]
  com.atlassian.confluence.macro.Macro$BodyType/PLAIN_TEXT)

(defn -getOutputType [_]
  com.atlassian.confluence.macro.Macro$OutputType/BLOCK)
