(ns jonmort.plugin.ring.ditaa
  (:use compojure.core)
  (:require [compojure.route :as route]
            [compojure.handler :as handler]
            [clojure.data.codec.base64 :as b64])
  (:import [org.stathissideris.ascii2image.graphics Diagram BitmapRenderer]
           [org.stathissideris.ascii2image.core ConversionOptions ProcessingOptions RenderingOptions]
           [org.stathissideris.ascii2image.text TextGrid]
           [javax.imageio ImageIO]
           [java.io ByteArrayOutputStream ByteArrayInputStream]
           [java.awt Color]))
; this turned out to not be needed but it is a nice function for turning hex strings into colours with an alpha
(defn parse-bg-color [hex-str]
  (if (Boolean/valueOf hex-str)
    (Color. 0 0 0 0)
    (if (= (count hex-str) 6)
      (Color/decode (str "0x" hex-str))
      (apply (fn [r g b a] (Color. r g b a))
             (for [v (partition 2 hex-str)]
               (Integer/parseInt v 16))))
    ))

(defn b-val [key params ]
  (or (Boolean/valueOf (key params)) false))

(defn double-val [k params default]
  (let [v (k params)]
    (try
      (Double/parseDouble (or v default))
      (catch Exception e default))))

(defn render-image [params]
  (let [options (ConversionOptions.)
        text (-> (.getBytes (:data params) "UTF-8")
                 (b64/decode)
                 (String. "UTF-8"))
        processingOptions (doto (.processingOptions options)
                            (.setAllCornersAreRound (b-val :rounded-corners params))
                            (.setTabSize (Integer/parseInt (or (:tabs params) "8")))
                            (.setPerformSeparationOfCommonEdges (not (b-val :no-separation params))))
        renderingOptions (doto (.renderingOptions options)
                           (.setScale (double-val :scale params 1.0))
                           (.setAntialias (not (b-val :no-antialias params)))
                           (.setDropShadows (not (b-val :no-shadows params))))
        grid (doto (TextGrid.)
               (.initialiseWithText text processingOptions))
        diagram (Diagram. grid options)]
    (.renderToImage (BitmapRenderer.) diagram renderingOptions)
    ))

(defn image-stream [image]
  (let [stream (ByteArrayOutputStream.)]
    (do
      (ImageIO/write image "png" stream)
      (ByteArrayInputStream. (.toByteArray stream)))))

(defroutes main-routes
  (ANY "/image" {params :params} {:headers {"Content-Type" "image/png"}
                                  :body (-> (render-image params)
                                            (image-stream))})
  (route/not-found "<h1>Page not found context</h1>")
  )

; this was inspired by leiningen.ring.war.generate-handler from war.clj https://github.com/weavejester/lein-ring/blob/master/src/leiningen/ring/war.clj
; in lein-ring.
(defn make-plugin-servlet-handler
  "Wraps a ring handler with the correct :context and :path-info inserted into the request.
   The plugins system does not set these correctly"
  [handler]
  (fn [request]
    (handler
      (assoc request
        :path-info (.getPathInfo (:servlet-request request))
        :context (.getContextPath (:servlet-request request))))))

(def app (handler/site (make-plugin-servlet-handler main-routes)))
